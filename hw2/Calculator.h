//
//  Calculator.h
//  CalculatorApp
//
//  Created by William McDermid on 8/31/14.
//  Copyright (c) 2014 William McDermid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject
{
    //public instance variables go here
    
}

//public properties go here
@property NSMutableString *toDisplay;

//public method signatures go here
-(void) clear;
-(void) numberPressed:(int) number;
-(void) operationPressed:(NSString*) operation;
-(void) equalPressed;
-(void) decimalPressed;
-(void) percentPressed;
-(void) signTogglePressed;
@end
