//
//  UserInfoViewController.m
//  hw2
//
//  Created by Martin Greenberg on 9/17/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "UserInfoViewController.h"

@interface UserInfoViewController ()
@property (strong, nonatomic) NSUserDefaults *userInfo;
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *favoriteFood;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;
- (IBAction)savePressed:(id)sender;
- (IBAction)clearPressed:(id)sender;
@end

@implementation UserInfoViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.userInfo = [NSUserDefaults standardUserDefaults];
	self.firstName.text = (NSString *)[self.userInfo objectForKey:@"firstName"];
	self.lastName.text = (NSString *)[self.userInfo objectForKey:@"lastName"];
	self.favoriteFood.text = (NSString *)[self.userInfo objectForKey:@"favoriteFood"];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)savePressed:(id)sender {
	[self.userInfo setObject:self.firstName.text forKey:@"firstName"];
	[self.userInfo setObject:self.lastName.text forKey:@"lastName"];
	[self.userInfo setObject:self.favoriteFood.text forKey:@"favoriteFood"];
}
- (IBAction)clearPressed:(id)sender{
	[NSUserDefaults resetStandardUserDefaults];
	self.userInfo = [NSUserDefaults standardUserDefaults];
	self.firstName.text = @"";
	self.lastName.text = @"";
	self.favoriteFood.text = @"";
}

@end
