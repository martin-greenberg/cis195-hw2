//
//  CoursesViewController.m
//  hw2
//
//  Created by Martin Greenberg on 9/17/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "CoursesViewController.h"

@interface CoursesViewController ()
@property (strong, nonatomic) IBOutlet UILabel *className;
@property (strong, nonatomic) IBOutlet UITextView *classDescription;
@property (strong, nonatomic) IBOutlet UIButton *relatedCourse;

@end

@implementation CoursesViewController

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	((CoursesViewController *) segue.destinationViewController).sender = sender;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	UIButton *senderButton = self.sender;
	NSString *classIdentifier = senderButton.titleLabel.text;
	[self.className setText:classIdentifier];
	if([classIdentifier isEqualToString:@"CIS-195"]){
		[self.classDescription setText:@"Prerequisites: CIS 120 and CIS 240.  Students should come with a strong understanding of the fundamentals of programming, especially the C language and memory management.\nThis project-oriented course is centered around application development on the iOS platform, Apple’s mobile operating system developed for the iPhone and other Apple mobile devices. The first half of the course will involve fundamentals of iPhone development, where students learn the Objective-C programming language for iOS, an object-oriented C dialect with Smalltalk-style messaging, as well as efficient memory management and event-based programming on the iOS platform.  In the second half of the course, students work in teams to conceptualize and develop a significant iPhone application. Creativity and originality are highly encouraged!"];
		self.relatedCourse.hidden = NO;
		[self.relatedCourse setTitle:@"CIS-190" forState:UIControlStateNormal];
	}
	if([classIdentifier isEqualToString:@"CIS-120"]){
		[self.classDescription setText:@"The goal of the course is to teach fundamental concepts of programming that will enable you to solve interesting, challenging real-world problems with reliable, modular programs that can be tested, extended, shared with others, and combined effectively with other programs. You will learn about data types and data abstraction, how data is represented in memory, how to decompose complex programming problems into manageable subproblems, how and when to use elementary data structures such as arrays, lists, trees, and maps, different approaches to structuring programs (object-oriented, imperative, functional), communication between programs and their environment (basic user interface, input/output, networked communication), and how to test and fix programs (unit testing, debugging). While this course focuses on the fundamentals of programming, we will use examples and assignments that give you a first taste of important areas of computer science, including computer graphics and multimedia – how to process and present images and sounds; databases and search – how to organize and search complex data; networking – how to manage communication among computers; artificial intelligence – how to write programs that find their way around and learn from experience; and games and simulation – how to model a complex world."];
		[self.relatedCourse setTitle:@"CIS-121" forState:UIControlStateNormal];
	}
	if([classIdentifier isEqualToString:@"CIS-240"]){
		[self.classDescription setText:@"You know how to program, but do you know how computers really work? How do millions of transistors come together to form a complete computing system? This bottom-up course begins with transistors and simple computer hardware structures, continues with low-level programming using primitive machine instructions, and finishes with an introduction to the C programming language. This course is a broad introduction to all aspects of computer systems architecture and serves as the foundation for subsequent computer systems courses, such as Digital Systems Organization and Design (CIS 371), Computer Operating Systems (CIS 380), and Compilers and Interpreters (CIS 341)."];
		[self.relatedCourse setTitle:@"CIS-371" forState:UIControlStateNormal];
	}
	if([classIdentifier isEqualToString:@"CIS-190"]){
		[self.classDescription setText:@"This course will provide an introduction to programming in C++ and is intended for students who already have some exposure to programming in another language such as Java, C++ provides the programmer with a greater level of control over machine resources and are commonly used in situations where low level access or performance are important. This course will illuminate the issues associated with programming at this level and will cover issues such as explicit memory management, pointers, the compilation process and debugging. The course will involve several programming projects which will provide students with the experience they need to program effectively in these languages. This course assumes programming experience equivalent to CIS 110, CIS 120 or ESE 112."];
	}
	if([classIdentifier isEqualToString:@"CIS-121"]){
		[self.classDescription setText:@"This is an introductory course about Basic Algorithms and Data Structures using the Java programming language. We introduce elementary concepts about the complexity of an algorithm and methods for analyzing the running time of software. We describe data structures like stacks, queues, lists, trees, priority queues, maps, hash tables and graphs, and we discuss how to implement them efficiently and how to use them in problems-solving software. A larger project introducing students to some of the challenges of software development concludes the course."];
		self.relatedCourse.hidden = YES;
	}
	if([classIdentifier isEqualToString:@"CIS-371"]){
		[self.classDescription setText:@"(Prerequisite(s): CIS 240, knowledge of at least one programming language). This is the second computer organization course and focuses on computer hardware design. Topics covered are: (1) basic digital system design including finite state machines, (2) instruction set design and simple RISC assembly programming, (3) quantitative evaluation of computer performance, (4) circuits for integer and floating-point arithmetic, (5) datapath and control, (6) micro-programming, (7) pipelining, (8) storage hierarchy and virtual memory, (9) input/output, (10) different forms of parallelism including instruction level parallelism, data-level parallelism using both vectors and message-passing multi-processors, and thread-level parallelism using shared memory multiprocessors. Basic cache coherence and synchronization."];
		self.relatedCourse.hidden = YES;
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
