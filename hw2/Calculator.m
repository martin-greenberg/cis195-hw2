//
//  Calculator.m
//  CalculatorApp
//
//  Created by William McDermid on 8/31/14.
//  Copyright (c) 2014 William McDermid. All rights reserved.
//

#import "Calculator.h"

@interface Calculator()

@property float currentValue;
@property float previousOperand;
@property int numbersEntered; //number of numbers entered past the decimal point
@property BOOL isInt; //if a decimal point is added then this is 0 otherwise 1
@property BOOL shouldClear;
@property BOOL lastWasEquals;
@property NSMutableString *currentOperation;


//private method signatures go here


@end

@implementation Calculator
-(id) init
{
	self = [super init];
	if (self)
	{
		_toDisplay = [[NSMutableString alloc] init];
		self.currentOperation = [[NSMutableString alloc] init];
		[self clear];
	}
	return self;
}

-(void) operationPressed:(NSString *)operation
{
	self.shouldClear = 1;
	if(![self.currentOperation isEqual:@"none"] && (self.lastWasEquals != 1)){
		[self equalPressed];
	}
	if(self.previousOperand == 0){
		self.previousOperand = self.currentValue;
	}
	[self.currentOperation setString:operation];
	self.lastWasEquals = 0;
	self.isInt = 1;
	self.numbersEntered = 0;
}

-(void) clear
{
	self.isInt = 1;
	self.numbersEntered = 0;
	self.currentValue = 0;
	self.previousOperand = 0;
	[self.currentOperation setString:@"none"];
	self.shouldClear = 0;
	self.lastWasEquals = 0;
	[_toDisplay setString:@"0"];
}

-(void) signTogglePressed
{
	if(self.currentValue != 0){self.currentValue *= -1;
		[_toDisplay setString:[NSString stringWithFormat:@"%g", self.currentValue]];
		self.shouldClear = 0;
	}
}
-(void) numberPressed:(int)number
{
	float numFloat = (float) number;
	if(!self.shouldClear){
		self.currentValue = self.isInt ? (numFloat+self.currentValue*10) :
									  (self.currentValue + numFloat/(pow(10,(self.numbersEntered+1))));
		self.numbersEntered += self.isInt ? 0 : 1;
	} else {
		self.previousOperand = self.currentValue;
		self.currentValue = numFloat;
		self.shouldClear = 0;
		self.isInt = 1;
		self.numbersEntered = 0;
		if(self.lastWasEquals) [self.currentOperation setString:@"none"]; //entering first operand, no operation
	}
	[_toDisplay setString:[NSString stringWithFormat:@"%g", self.currentValue]];
}

-(void) decimalPressed
{
	if(self.isInt){
		self.isInt = 0;
		if(!self.shouldClear){
			[_toDisplay appendString:@"."];
		} else {
			self.currentValue = 0;
			[_toDisplay setString:@"0."];
		}
	}
	self.shouldClear = 0;
}

-(void) percentPressed
{
	self.currentValue=self.currentValue/100.0;
	if(![self.currentOperation isEqual:@"none"]){
		[self equalPressed];
	}
	self.lastWasEquals = 0; //does not behave like an equals operation
	self.shouldClear = 1;
	[_toDisplay setString:[NSString stringWithFormat:@"%g", self.currentValue]];
}

-(void) equalPressed
{
	int secondOperand = self.currentValue;
	if([self.currentOperation isEqual:@"+"]){
		self.currentValue+=self.previousOperand;
	} else {
		if([self.currentOperation isEqual:@"-"]){
			if(!self.lastWasEquals){
				self.currentValue = self.previousOperand - self.currentValue;
			} else {
				self.currentValue -= self.previousOperand;
			}
		} else {
			if([self.currentOperation isEqual:@"X"]){
				self.currentValue *= self.previousOperand;
			} else {
				if([self.currentOperation isEqual:@"/"]){
					if(self.currentValue == 0){
						[self.toDisplay setString:@"DIV/0"];
						self.shouldClear = 1;
						return;
					} else {
						if(!self.lastWasEquals){
							self.currentValue = self.previousOperand / self.currentValue;
						} else {
							self.currentValue = self.currentValue/self.previousOperand;
						}
					}
				} else {
					NSLog(@"Error in equalPressed");
				}
			}
		}
	}
	self.shouldClear = 1;
	[_toDisplay setString:[NSString stringWithFormat:@"%g", self.currentValue]];
	if(!self.lastWasEquals){
		self.lastWasEquals = 1;
		self.previousOperand = secondOperand;
	}
}
@end
