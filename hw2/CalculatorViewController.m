//
//  CalculatorViewController.m
//  CalculatorApp
//
//  Created by William McDermid on 8/31/14.
//  Copyright (c) 2014 William McDermid. All rights reserved.
//

#import "CalculatorViewController.h"
#import "Calculator.h"

@interface CalculatorViewController ()

@property (strong, nonatomic) IBOutlet UILabel *calculatorDisplay;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *functions;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *operations;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *numbers;
@property (strong, nonatomic) Calculator *myCalc;

- (void)addButtonBordersToCalculatorButtons:(NSArray *)buttons;

- (IBAction)numberPressed:(id)sender;
- (IBAction)operationPressed:(id)sender;
- (IBAction)clearPressed:(id)sender;
- (IBAction)percentPressed:(id)sender;
- (IBAction)decimalPressed:(id)sender;
- (IBAction)equalPressed:(id)sender;
- (IBAction)togglePressed:(id)sender;
@end

@implementation CalculatorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	self.myCalc = [[Calculator alloc] init];
	[self.myCalc clear];
    [self addButtonBordersToCalculatorButtons:self.operations];
    [self addButtonBordersToCalculatorButtons:self.functions];
    [self addButtonBordersToCalculatorButtons:self.numbers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initialization

- (void)addButtonBordersToCalculatorButtons:(NSArray *)buttons
{
    for (UIButton *button in buttons) {
        [[button layer] setBorderWidth:0.25f];
        [[button layer] setBorderColor:[UIColor blackColor].CGColor];
    }
}

#pragma mark - IBActions

- (IBAction)numberPressed:(id)sender
{
    UIButton *buttonPressed = (UIButton *)sender;
	[self.myCalc numberPressed:(float)[buttonPressed.titleLabel.text intValue]];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
    NSLog(@"%@", buttonPressed.titleLabel.text);
}

- (IBAction)operationPressed:(id)sender
{
    UIButton *buttonPressed = (UIButton *)sender;
	[self.myCalc operationPressed:buttonPressed.titleLabel.text];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
    NSLog(@"%@", buttonPressed.titleLabel.text);
}

- (IBAction)clearPressed:(id)sender
{
    UIButton *buttonPressed = (UIButton *)sender;
	[self.myCalc clear];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
    NSLog(@"%@", buttonPressed.titleLabel.text);
}
- (IBAction)percentPressed:(id)sender
{
	UIButton *buttonPressed = (UIButton *)sender;
	[self.myCalc percentPressed];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
	NSLog(@"%@", buttonPressed.titleLabel.text);
}
- (IBAction)decimalPressed:(id)sender
{
	UIButton *buttonPressed = (UIButton *)sender;
	[self.myCalc decimalPressed];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
	NSLog(@"%@", buttonPressed.titleLabel.text);
}
- (IBAction)equalPressed:(id)sender
{
	UIButton *buttonPressed = (UIButton *)sender;
	[self.myCalc equalPressed];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
	NSLog(@"%@", buttonPressed.titleLabel.text);
}
- (IBAction)togglePressed:(id)sender
{
	[self.myCalc signTogglePressed];
	[self.calculatorDisplay setText:self.myCalc.toDisplay];
}
	@end
