//
//  ClassListController.m
//  hw2
//
//  Created by Martin Greenberg on 9/17/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "ClassListController.h"
#import "CoursesViewController.h"

@interface ClassListController ()
@end

@implementation ClassListController

- (void)viewDidLoad {
	[super viewDidLoad];
	NSLog(@"test1\n");
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	((CoursesViewController *) segue.destinationViewController).sender = sender;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end